class TableNotFoundException(Exception):
    pass


class TableFailedtoAddException(Exception):
    pass


"""
The buffer pool class represents our in memory data store.
We will handle the tables that are represented on disk here in memory. 
If needed, will be able to grab data from disk via transaction
"""


class BufferPool:
    def __init__(self):
        self._BP = {}
        pass

    def add_table(self, newTable):
        """
        Adds new table to the buffer pool.
        :param newTable: new table to be added
        :return: 200 if pass, 500 if failed.
        """
        if self.evict_strategy():
            self._BP[newTable.get_tablename()] = newTable
            return 200
        else:
            raise TableFailedtoAddException("Evict strategy failed. Cannot add Table into BP")

    def evict_strategy(self):
        """
        Eviction strategy for our buffer pool
        TODO: should check if we can evict based on FIFO. If evicted, immediately log.
        :return: True if we have space to add new table, False if we were not able to add new table.
        """
        return True

    def get_table(self, tablename):
        if self._BP[tablename]:
            return self._BP[tablename]
        else:
            raise TableNotFoundException("Cannot find table in Buffer pool")

    def get_bp_size(self):
        """
        get the buffer pool size in terms of tables
        :return: size of buffer pool
        """
        return len(self._BP)

    def print_bp(self):
        print(self._BP)

    def reset(self):
        self._BP = {}
