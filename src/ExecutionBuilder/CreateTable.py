from src.ExecutionBuilder.ExecutionNode import ExecutionNode
from src.datatypes.DataTypes import DataTypes
from src.Table.Column import *


class TableNameSyntaxError(Exception):
    pass


class ColumnDefSyntaxError(Exception):
    pass


class CreateTable(ExecutionNode):
    def __init__(self, tablename, column_defs, fs):
        ExecutionNode.__init__(self)
        self.tablename = tablename
        self.column_defs = column_defs.split(',')
        self.columns = []
        self.filesystem = fs
        # by default, parent and children of create table should be NONE
        self.parent = None
        self.children = None

    def validate(self):
        """
        validates if column definitions are correct. Tablename can be anything
        :return: True if valid, raise exception if false.
        """
        if len(self.tablename.strip().split(' ')) > 1:
            raise TableNameSyntaxError("Tablenames can only be 1 word")
        else:
            for col in self.column_defs:
                ind_col = col.strip().split(' ')
                if len(ind_col) > 2:
                    raise ColumnDefSyntaxError("Column incorrectly specified")
                else:
                    if not DataTypes.has_value(ind_col[1]):
                        raise ColumnDefSyntaxError("Data type not supported or spelled incorrectly."
                                                   "supported types are: {0}".format([e.value for e in DataTypes]))
                    else:
                        self.columns.append(Column(ind_col[0], ind_col[1]))
        return True

    def execute(self):
        """
        Runs the create table command.
        :return: True if pass, False if failed.
        """
        self.validate()
        return self.filesystem.create_table(self.tablename, self.columns)

    def __repr__(self):
        out = "type: {0}, tablename: {1}, columns: {2}"
        return out.format(self.type, self.tablename, self.columns)
