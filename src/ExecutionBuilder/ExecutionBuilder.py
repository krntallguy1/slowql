from src.ExecutionBuilder.TableScan import TableScanNode
from src.ExecutionBuilder.CreateTable import CreateTable
from src.ExecutionBuilder.QueryContext import QueryContext
from src.ExecutionBuilder.ShowTables import ShowTables
from src.ExecutionBuilder.InsertInto import InsertInto
from src.ExecutionBuilder.Operator import Operator
from src.ExecutionBuilder.OrderBy import OrderBy

import logging

from src.core.SlowQLInterpreter import IncorrectSyntaxException

import re


class IllogicalInputProvided(Exception):
    pass


class ColumnNotFoundException(Exception):
    pass


class ExecutionBuilder:
    """
    Workhorse Execution build class. Works with Expressions built from ParserExpressions
    Each Select Statement should have its own Execution Builder for sake of simplicity.

    SlowQLFS will be directly referenced by each ExecutionBuilder.

    TODO: how to handle atomicity of FS here. Give more thought in refactoring

    Set runtime rules here.
    ====================================================================================
    Query related:
    - Select
    - From
    - Where
    - Join (left, inner, right, outer)
    - On
    - Group By

    Data Definition related:
    - Create Table

    Data Modification related:
    - Insert Into
    """

    def __init__(self, expression_chain, fs):
        self.logger = logging.getLogger(__name__)
        self.expression_head = expression_chain
        self.logger.info("Expression Chain for SQL is: {0}".format(self.get_expression()))
        self.filesystem = fs
        self._type = self.set_type()

    ################### Initialization/Validation ####################
    #                                                                #
    #           On execution builder creation methods                #
    #                                                                #
    ##################################################################

    def set_type(self):
        """
        Sets what type of execution we will be doing depending on the start of the input.
        We have validation here that says if we get a static expressions or random active expression,
        we will raise an error
        :return: str type of execution
        """
        exp = self.expression_head.get_type()
        if exp == 'select':
            return 'SQL'
        elif exp == 'insert_into':
            return 'DML'
        elif exp == 'create_table':
            return 'DDL'
        elif exp == 'show_tables':
            return 'show_tables'
        else:
            raise IllogicalInputProvided('The input is not valid. Please try again')

    ######################### Builder Methods ########################
    #                                                                #
    #           Builder help methods. Workhorse logic                #
    #                                                                #
    ##################################################################

    def build(self):
        """
        Takes the expression chain and translates it into tree of expression nodes.
        :return: ExpressionNode head of tree
        """
        if self._type == 'SQL':
            self.logger.info("Built SQL Tree from Expression Chain")
            return self.build_sql(self.expression_head)
        elif self._type == 'DML':
            self.logger.info("Successfully built DML")
            return self.build_dml()
        elif self._type == 'DDL':
            self.logger.info("Successfully built DDL")
            return self.build_ddl()
        elif self._type == 'show_tables':
            return self.build_show_tables()

    def build_sql(self, exp_head):
        """
        SQL builder.

        TODO: Handle subqueries by combining contexts in the future

        Assume we are to handle ONE select at a time. Build out SQL from there.
        :return: ExecutionNode head of tree
        """
        context = QueryContext()
        head = exp_head.get_next()

        # handle projections first
        context = self.add_projections(context, head.get_input())

        # generic handler for rest of query
        context = self.add_treenodes(context, exp_head)
        return context

    def build_dml(self):
        """
        Insert Into clause
        TODO: handle updates later
        :return: ExecutionNode InsertInto
        """
        tablename, data = self.parse_insert_into(self.expression_head)
        return InsertInto(self.filesystem, tablename, data)

    def build_show_tables(self):
        """
        Unique Show Tables command.
        :return: ExecutionNode for Show Tables
        """
        return ShowTables(self.filesystem)

    def build_ddl(self):
        """
        Build table here.
        always call validate while building
        Guarantees: [create_table node] => [static node]
        The static node holds: tablename + (column definitions) <= IN THIS ORDER
        :return: ExecutionNode for create table
        """
        ct = self.create_table(self.expression_head)
        return ct

    ###################### Internal Handlers #########################
    #                  Handles Nodes peripherally                    #
    #                                                                #
    ##################################################################

    def add_treenodes(self, context, exp_head):
        """

        :param context: context for query
        :param exp_head: head of expression
        :return: context after modification
        """
        travelnode = exp_head
        while travelnode is not None:
            if travelnode.get_type() == 'from':
                context, _ = self.create_tablescan(travelnode, context)
            if 'join' in travelnode.get_type():
                join_type = travelnode.get_type()
                context, join_tab = self.create_tablescan(travelnode, context)
                travelnode = travelnode.get_next().get_next()
                context.add_join(join_tab, self.parse_on(travelnode), join_type)
            if 'where' in travelnode.get_type():
                self.parse_where(travelnode, context)
            if 'order_by' in travelnode.get_type():
                self.parse_order_by(travelnode, context)
            travelnode = travelnode.get_next()
        return context

    def parse_where(self, node, context):
        """
        Protocol for parsing WHERE clause.
        can be one of two methods:
        1. [where] => [static] => [operator] => [static]
        2. [where] => [static] => [operator] => [subquery]

        :param node: ExpressionNode either Static node or Select node
        :param context: context
        :return: context
        """
        where_ctx = self.get_where_context(node)
        context.add_operator(Operator(where_ctx))
        return context

    def get_where_context(self, node):
        travelnode = node.get_next()
        static_left = travelnode.get_input()
        travelnode = travelnode.get_next()
        op_type = travelnode.get_optype()
        travelnode = travelnode.get_next()
        if travelnode.get_type() == 'static':
            return static_left, op_type, travelnode.get_input()
        elif travelnode.get_type() == 'subquery':
            return static_left, op_type, self.build_sql(travelnode.get_subquery()).build()

    def add_projections(self, context, projections):
        """
        Parses and adds projections to the context
        :param context: context for query
        :param projections: str parsed input for projections
        :return: context after modification
        """
        cols = projections.split(',')
        for col in cols:
            if '.' in col:
                tab_col = col.split('.')
                context.add_projection(tab_col[1].strip(), tab_col[0].strip())
            else:
                context.add_projection(col.strip())
        return context

    def create_table(self, node_head):
        """
        parses create table expressionnode

        TWO ways this can be created:
        1) create table DANIEL(age int, name str)
        2) create table DANIEL (age int, name str)

        use regex to parse through:


        :param node_head: Create_Table Expression node
        :return: CreateTable builder node
        """
        tablename_coldefs = node_head.get_next()
        string = tablename_coldefs.get_input()
        info = re.match(r'([\w]+[ ]*)\(([\w, ]*)\)', string)
        if not info:
            raise IncorrectSyntaxException
        tablename = info.group(1)
        col_defs = info.group(2)
        return CreateTable(tablename, col_defs, self.filesystem)

    def create_tablescan(self, node, context):
        """
        :param node: head node
        :param context: context of sql
        :return: tuple of tablename and the context
        """
        node_tablename = node.get_next()
        tablename = node_tablename.get_input()
        ts = TableScanNode(tablename, self.filesystem)
        ts.validate()
        context.add_tablescans(tablename, ts)
        return context, tablename

    def parse_on(self, node):
        """
        handles ON expression node. Grabs columns to join on and passes to context
        TODO: Add as operator instead.
        :param node: ExecutionNode representing ON
        """
        node_on = node.get_next()
        return list(map(str.strip, node_on.get_input().split(',')))

    def parse_insert_into(self, node):
        """
        Parses the input for insert into. Grabs static node following insert into node
        and splits into tuple => (tablename, data)
        :param node: insert into expression node
        :return: tuple (tablename, data)
        """
        node_ii = node.get_next()
        ii_info = node_ii.get_input()
        info = re.match(r'([\w]+[ ]*)\(([\w., ]*)\)', ii_info)
        if not info:
            raise IncorrectSyntaxException
        tablename = info.group(1)
        col_data = info.group(2)
        return tablename, col_data

    def parse_order_by(self, node, context):
        """
        Takes a order by node and adds the clause into the sql context
        :param node: order by node
        :param context: sql context
        :return: sql context
        """
        order_key_node = node.get_next()
        keys = order_key_node.get_input()
        order_by = OrderBy(keys)
        context.add_order_by(order_by)
        return context

    ########################## Utilities #############################
    #                         other utils                            #
    #                                                                #
    ##################################################################

    def get_expression(self):
        """
        Returns the expression chain in a nicely formatted way
        :return: str [node type] => [node type] => etc...
        """
        head = self.expression_head
        out_string = "[{0}]".format(head.get_type())
        head = head.get_next()
        while head is not None:
            out_string = "{0} => [{1}]".format(out_string, head.get_type())
            head = head.get_next()
        return out_string
