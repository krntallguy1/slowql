class ExecutionNode:
    """
    Base generic class for sql execution. Serves as the building blocks for
    the execution builder.
    """

    def __init__(self):
        self.type = None
        self.children = None
        self.parent = None
        self.workingset = None

    def validate(self):
        pass

    def execute(self):
        """
        This method should check if the node has children, if possible,
        run child's execute first.
        :return: ResultSet
        """
        pass

    def get_type(self):
        return self.type
