from src.ExecutionBuilder.ExecutionNode import ExecutionNode
from src.BufferPool.BufferPool import TableNotFoundException


class InsertInto(ExecutionNode):
    def __init__(self, fs, tablename, data):
        ExecutionNode.__init__(self)
        self.filesystem = fs
        self.tablename = tablename
        self.data = data
        self.table = None

    def validate(self):
        """
        validates the insert into. matching tablename and data types
        :return: True if valid, False if not valid
        """
        try:
            self.filesystem.get_table(self.tablename)
        except TableNotFoundException:
            return False
        return True

    def execute(self):
        """
        Runs the insert into the table
        """
        if self.validate():
            self.filesystem.insert_row(self.tablename, self.data)
