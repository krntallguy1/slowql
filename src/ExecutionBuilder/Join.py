from src.ExecutionBuilder.ExecutionNode import ExecutionNode


class Join(ExecutionNode):
    """
    Join execution node. This class takes two tablescans and joins them based on
    the column references given by the `on` clause.

    PARENT CLASS OF TABLESCANS.
    """

    def __init__(self, join_type, tab1, tab2, link_cols):
        """
        Join execution node
        :param join_type: str (outer, inner, etc)
        :param tab1: ExecutionNode left side
        :param tab2: ExecutionNode right side
        :param link_cols: list[str] cols to join on
        """
        ExecutionNode.__init__(self)
        self.type = 'join'
        self.join_type = join_type
        self.link_cols = link_cols
        self.parent = None
        self.children = (tab1, tab2)

    def validate(self):
        """
        validates the join on the columns specified by the ON clause.
        :return:
        """
        pass

    def execute(self):
        """
        Takes the results from the tablescans, and performs HASH join (by default)
        :return: resultset of join.
        """
        return self.join(self.children[0].execute(), self.children[1].execute())

    def join(self, rs1, rs2):
        """
        Joining two results sets based on the linked cols
        :param rs1: ResultSet left side
        :param rs2: ResultSet right side
        :return: ResultSet output
        """
        return rs1.hash_join(rs2, self.link_cols, self.join_type)
