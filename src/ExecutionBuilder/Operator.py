from src.ExecutionBuilder.ExecutionNode import *
from src.ExecutionBuilder.operations.GreaterThan import GreaterThan
from src.ExecutionBuilder.operations.LessThan import LessThan
from src.ExecutionBuilder.operations.EqualTo import EqualTo


class ResultsNotFoundException(Exception):
    pass


class InvalidOperatorException(Exception):
    pass


class Operator(ExecutionNode):
    """
    Base class for operators.
    Should abstract one layer to allow for different types of operators:
    Arithmetic, Bitwise, Comparison

    TODO: Phase 1 will be implementing comparison. Then arithmetic and bitwise
    EX)
    1. A = B
    2. A > B
    3. A < B
    """

    def __init__(self, where_ctx):
        ExecutionNode.__init__(self)
        self.type = 'operator'
        self.op = self.parse_operator(where_ctx)
        self.children = None
        self.parent = None

    def execute(self):
        """
        First get result set of child's execute, then perform execution here.
        :return: ResultSet
        """
        is_tuple = isinstance(self.children, tuple)
        if is_tuple:
            filter_rs = self.children[0].execute()
        else:
            filter_rs = self.children.execute()
        return self.op.execute(filter_rs)

    def parse_operator(self, where_ctx):
        """
        Parses the SQL input and creates BaseOperator object
        :param where_ctx tuple holds all context for the where clause
        :return: BaseOperator operator object
        """
        if where_ctx[1] == '>':
            return GreaterThan(where_ctx[0], where_ctx[2])
        elif where_ctx[1] == '<':
            return LessThan(where_ctx[0], where_ctx[2])
        elif where_ctx[1] == '=':
            return EqualTo(where_ctx[0], where_ctx[2])

    def get_operation(self):
        """
        returns operation
        :return: BaseOperator
        """
        return self.op

    def set_children(self, left):
        right = self.get_operation().get_right()
        if isinstance(right, ExecutionNode):
            self.children = left, right
        else:
            self.children = left
