from src.ExecutionBuilder.ExecutionNode import ExecutionNode


class OrderBy(ExecutionNode):
    def __init__(self, keys):
        ExecutionNode.__init__(self)
        self.type = 'order by'
        self.order_keys = keys.split(',')

    def execute(self):
        return self.children.execute().order_by(self.order_keys)
