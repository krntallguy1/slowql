from src.ExecutionBuilder.Join import Join
import logging


class ColumnDoesNotExist(Exception):
    pass


class TableScanNotFound(Exception):
    pass


class InvalidJoinException(Exception):
    pass


class QueryContext:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        # PROJECTIONS: {"TABLENAME" : ["colname1", "colname2"]}
        self.projections = {}
        # TABLESCANS: {"TABLENAME" : TableScan()}
        self.tablescans = {}
        # OPERATORS: []
        self.operators = []
        # currently keeping a list of joins for debug purposes
        self.joins = []
        self.left = None

    ####################### Context Work Methods ###########################
    #                Work Horse Methods. Adds to Context                   #
    #                                                                      #
    ########################################################################

    def add_operator(self, operator):
        """
        Adds operators to context
        :param operator ExecutionNode
        """
        self.operators.append(operator)
        operator.set_children(self.left)
        self.left.parent = operator
        self.left = operator

    def verify_projection(self, colname):
        """
        Sets projection as valid
        :param colname: str name of column projection
        """
        pass

    def add_projection(self, colname, tablename='default'):
        """
        Add a new projection given column name.
        TODO: deal with ambiguity clause later.
        :param colname: str name of column provided by sql
        """
        if tablename in self.projections:
            self.projections[tablename].append(colname)
        else:
            self.projections[tablename] = [colname]

    def get_final_projection_cols(self):
        """
        Gets a list of all final column projections
        :return: list[str] final projections
        """
        final_proj = []
        for key, val in self.projections.items():
            for col in val:
                final_proj.append(col)
        return final_proj

    def add_tablescans(self, tablename, executionnode):
        """
        Add a new tablescan to context
        TODO: for now, we will set the initial TS projections as *. In future, we should prune intelligently
        :param tablename: str name of table
        :param executionnode: ExecutionNode for the tablescan
        """
        executionnode.set_columns('*')
        self.tablescans[tablename] = executionnode
        if self.left is None:
            self.left = executionnode

    def add_join(self, right_side, link_col, join_type):
        """
        creates logical link between two tablescans for join
        :param right_side: ExecutionNode RIGHT side of join
        :param link_col: list[str] cols to link the joins
        :param join_type: str type of join
        """
        try:
            right = self.tablescans[right_side]
        except TableScanNotFound:
            raise
        if self.left is None:
            raise InvalidJoinException("Join Sides cannot be None")
        else:
            self.left = Join(join_type, self.left, right, link_col)
            self.joins.append(self.left)

    def add_order_by(self, order_by_node):
        """
        Adds a new order by to context. Should be appended to highest part of tree...
        :param order_by_node: ExecutionNode order by node created in execution builder
        """
        order_by_node.children = self.left
        self.left = order_by_node

    ####################### Context Public Methods #########################
    #                      To Build and Execute                            #
    #                                                                      #
    ########################################################################

    def build(self):
        """
        build final tree
        :return: head of tree
        """
        return self.left

    def execute(self):
        """
        kick off query
        :return:
        """
        self.logger.info("Executing SQL:\n {0}".format(self.show_tree(self.left, 0, '')))
        final_rs = self.left.execute()
        return final_rs.project(self.get_final_projection_cols())

    ####################### Context Utility Methods ########################
    #                         General Utility                              #
    #                                                                      #
    ########################################################################

    def print_context(self):
        print("========================= QUERY CONTEXT ============================")
        print("The columns to Project: {0}".format(self.projections))
        print("Tablescans: {0}".format(self.tablescans))
        print("Joins: {0}".format(self.joins))
        print("Operators: {0}".format(self.operators))

    def show_tree(self, node, level, output):
        """
        pretty print tree
        """
        buffer = ''
        end = ''
        for x in range(0, level):
            buffer += '     '
        if level != 0:
            end = '|---> '
        if node is None:
            return output
        output += buffer + end + node.get_type() + '\n'
        if type(node.children) is tuple:
            for child in node.children:
                output = self.show_tree(child, level + 1, output)
        else:
            output = self.show_tree(node.children, level + 1, output)
        return output
