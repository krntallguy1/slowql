from src.ExecutionBuilder.ExecutionNode import ExecutionNode


class ShowTables(ExecutionNode):
    def __init__(self, fs):
        ExecutionNode.__init__(self)
        self.filesystem = fs

    def execute(self):
        """
        Runs the show tables command.
        Prints tables to console.
        """
        return self.filesystem.show_tables()
