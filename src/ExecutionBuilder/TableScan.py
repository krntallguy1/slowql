from src.ExecutionBuilder.ExecutionNode import ExecutionNode
from src.ResultSet.ResultSet import ResultSet


class TableNotFoundException(Exception):
    pass


class TableScanNode(ExecutionNode):
    """
    TableScan execution node. This class is the translation between interpreted
    SlowQL SQL and the execution expressions.

    This class will serve to optimize, execute, and runtime validate sql.
    """

    def __init__(self, tablename, fs, columns=None):
        ExecutionNode.__init__(self)
        self.type = 'tablescan'
        self.tablename = tablename
        self.filesystem = fs
        self.columns = columns
        self.parent = None
        # Tablescan children should always be none.
        self.children = None

    def validate(self):
        """
        validates the tablescan requirements. Raise exception if table not found or columns do not match.
        :return: True if valid, False if not valid
        """
        table = self.filesystem.get_table(self.tablename)
        if not table:
            raise TableNotFoundException("The table: {0} specified does not exist"
                                         .format(self.tablename))
        else:
            return True

    def execute(self):
        """
        Returns the TableScan data
        :return: ResultSet: data in table
        """
        return ResultSet(self.filesystem.scan(self.tablename), self.tablename)

    def set_columns(self, column_defs):
        """
        Set column definitions
        :param column_defs: string of column defs
        """
        self.columns = column_defs

    def get_columns(self):
        """
        return columns
        :return: list of column names
        """
        return self.columns

    def get_tablename(self):
        return self.tablename

    def __str__(self):
        out = "type: {0}, tablename: {1}, columns: {2}"
        return out.format(self.type, self.tablename, self.columns)
