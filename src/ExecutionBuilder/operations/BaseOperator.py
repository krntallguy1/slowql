from src.ExecutionBuilder.ExecutionNode import ExecutionNode
import ast


class IncompatibleRightSideOperator(Exception):
    pass


class BaseOperator(object):
    """
    Generic operator class that will resemble an operator plan
    """

    def __init__(self):
        self.type = None
        self.left = None
        self.right = None
        self.filter_rs = None

    def execute(self, filter_rs):
        self.filter_rs = filter_rs
        if isinstance(self.right, ExecutionNode):
            right = self.right.execute()
            if len(right) > 1:
                raise IncompatibleRightSideOperator("Right side of Operator is larger than 1 value")
            else:
                self.right = right[0]
        else:
            try:
                self.right = ast.literal_eval(self.right)
            except ValueError:
                pass

    def get_left(self):
        return self.left

    def get_right(self):
        return self.right
