from src.ExecutionBuilder.operations.BaseOperator import BaseOperator


class EqualTo(BaseOperator):
    """
    Greater Than operator. if '=' is in the operator, this class gets created
    """

    def __init__(self, left, right):
        BaseOperator.__init__(self)
        self.type = 'equal_to'
        self.left = left
        self.right = right

    def execute(self, filter_rs):
        """
        Takes a ResultSet, and filters on equal to key
        :param filter_rs: result set to filter
        :return: ResultSet
        """
        super(EqualTo, self).execute(filter_rs)
        return self.filter_rs.filter_equal(self.left, self.right)
