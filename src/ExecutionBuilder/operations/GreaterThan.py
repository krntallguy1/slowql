from src.ExecutionBuilder.operations.BaseOperator import BaseOperator


class GreaterThan(BaseOperator):
    """
    Greater Than operator. if '>' is in the operator, this class gets created
    """

    def __init__(self, left, right):
        BaseOperator.__init__(self)
        self.type = 'greater_than'
        self.left = left
        self.right = right

    def get_right(self):
        return self.right
