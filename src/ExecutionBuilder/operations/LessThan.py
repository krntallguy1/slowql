from src.ExecutionBuilder.operations.BaseOperator import BaseOperator


class LessThan(BaseOperator):
    """
    Greater Than operator. if '<' is in the operator, this class gets created
    """

    def __init__(self, left, right):
        BaseOperator.__init__(self)
        self.type = 'less_than'
        self.left = left
        self.right = right
