class InterpretError(Exception):
    pass


class Active_Expression:
    """
    Generic Active Expression class. Will use in interpreter to validate and transform SQL into
    execution tree language.

    Currently, we will support:
    - SELECT
    - FROM
    - WHERE
    - CREATE TABLE
    - INSERT INTO
    - SHOW TABLES
    """

    def __init__(self):
        self.expect_chain = None
        self.materialized = None
        self.keyword = None
        self.option = []
        self.validated = False

    def get_type(self):
        return self.keyword

    def validate(self, expression):
        """
        Takes expectation chain and compares to current chain.
        :return: True if validated, False if not.
        """
        check_exp = expression
        # edge case for hanging active expression case.
        if check_exp is None:
            raise InterpretError("This SQL is formatted incorrectly. Please try again")

        # logic to iterate through each link in expectation chain and compare to our
        # expression chain.
        for expected in self.expect_chain:
            valid = False
            if isinstance(expected, set):
                if check_exp.get_type() in expected:
                    check_exp.validated = True
                    valid = True
            elif check_exp.get_type() == expected:
                valid = True
                check_exp.validated = True
            if valid:
                check_exp = check_exp.get_next()
            else:
                raise InterpretError("This SQL is formatted incorrectly. Please try again")

        # conditionally, if we specify optional active expressions, we handle error check here.
        if len(self.option) != 0 and check_exp is not None:
            if check_exp.get_type() not in self.option:
                raise InterpretError("This SQL is formatted incorrectly. Please try again")
            else:
                check_exp.validated = True
                self.option = []
        return True

    def expect(self):
        """
        returns the expected clause chain
        :return: list of clauses
        """
        return self.expect_chain

    def append(self, expression):
        """
        append new expression to this current one. Run validate to see if appropriate clause has been added.
        If not, throw InterpretError, if yes, remove from expect chain, append to materialized chain
        :param expression: new expression to add
        :return: 200 if pass, 500 if fail
        """
        self.materialized = expression
        return self

    def get_next(self):
        return self.materialized

    def __repr__(self):
        """
        From this node, iterate until we get to end.
        :return:
        """
        out = "[{0}]".format(self.get_type())
        node = self.get_next()
        while node:
            out = "{0} => [{1}]".format(out, node.get_type())
            node = node.get_next()
        return out
