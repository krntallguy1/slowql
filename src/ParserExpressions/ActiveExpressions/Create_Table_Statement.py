from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class Create_Table(Active_Expression):
    """
    Expression class for Create Table
    """

    def __init__(self):
        Active_Expression.__init__(self)
        self.keyword = 'create_table'
        self.expect_chain = ['static']
