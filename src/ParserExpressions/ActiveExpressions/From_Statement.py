from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class From(Active_Expression):
    def __init__(self):
        Active_Expression.__init__(self)
        self.keyword = 'from'
        self.expect_chain = ['static']
        self.option = ['where', 'inner_join', 'outer_join', 'full_join', 'order_by']
