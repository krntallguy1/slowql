from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class Insert_Into(Active_Expression):
    """
    Expression class for Inserting Into
    """

    def __init__(self):
        Active_Expression.__init__(self)
        self.keyword = 'insert_into'
        self.expect_chain = ['static']
