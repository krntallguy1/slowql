from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class Join(Active_Expression):
    """
    Join class.

    types can be: inner, outer, full
    """

    def __init__(self, join_type):
        Active_Expression.__init__(self)
        self.keyword = join_type + '_join'
        self.expect_chain = ['static', 'on']
