from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class On(Active_Expression):
    """
    On expression that follows Joins
    """

    def __init__(self):
        Active_Expression.__init__(self)
        self.keyword = 'on'
        self.expect_chain = ['static']
        self.option = ['where', 'inner_join', 'outer_join', 'full_join']
