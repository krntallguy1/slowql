from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class Operator(Active_Expression):
    """
    Operator Expression:
    1. >
    2. <
    3. =
    """

    def __init__(self, op_type):
        Active_Expression.__init__(self)
        self.keyword = 'operator'
        self.expect_chain = [{'static', 'subquery'}]
        self.option = ['where', 'inner_join', 'outer_join', 'full_join']
        self.op_type = op_type

    def get_optype(self):
        return self.op_type
