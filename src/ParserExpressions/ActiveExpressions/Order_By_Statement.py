from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class Order_By(Active_Expression):
    """
    Order By clause
    """

    def __init__(self):
        Active_Expression.__init__(self)
        self.keyword = 'order_by'
        self.expect_chain = ['static']
