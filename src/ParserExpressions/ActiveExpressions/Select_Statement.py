from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class Select(Active_Expression):
    """
    Expression class for Select statement
    """

    def __init__(self):
        Active_Expression.__init__(self)
        self.keyword = 'select'
        # expect is minimum set of expressions to make interpretation possible
        self.expect_chain = ['static', 'from']
        self.option = []
