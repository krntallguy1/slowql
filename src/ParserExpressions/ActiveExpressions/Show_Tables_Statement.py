from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class Show_Tables(Active_Expression):
    """
    Expression class for show tables
    """

    def __init__(self):
        Active_Expression.__init__(self)
        self.keyword = 'show_tables'
        self.expect_chain = None

    def validate(self, expression):
        """
        Show tables has its own validate because it is unique. Don't want to create edge case
        in the generic validate steps. This will be best practice moving forward.
        :param expression: expression (if added)
        :return: InterpretError if there exists more than show tables, True if valid.
        """
        check_exp = expression
        if check_exp is not None:
            raise InterpretError("This SQL is formatted incorrectly. Please try again")
        else:
            return True
