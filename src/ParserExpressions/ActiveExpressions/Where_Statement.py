from src.ParserExpressions.ActiveExpressions.Active_Expressions import *


class Where(Active_Expression):
    """
    Where expression
    """

    def __init__(self):
        Active_Expression.__init__(self)
        self.keyword = 'where'
        self.expect_chain = ['static', 'operator']
        self.option = []
