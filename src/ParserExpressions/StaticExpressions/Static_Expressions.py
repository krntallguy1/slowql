class Static_Expression:
    """
    Generic Static Expression class. This expression class is used to represent strings.
    """

    def __init__(self, input):
        self.static_input = input
        self.type = 'static'
        self.expect = None
        self.materialized = None
        self.validated = False

    def get_type(self):
        return self.type

    def append(self, expression):
        self.materialized = expression
        return self

    def get_next(self):
        return self.materialized

    def get_input(self):
        return self.static_input
