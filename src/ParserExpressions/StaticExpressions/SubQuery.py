from src.ParserExpressions.StaticExpressions.Static_Expressions import *


class SubQuery(Static_Expression):
    def __init__(self, input):
        Static_Expression.__init__(self, input)
        self.type = 'subquery'
        self.subquery = None

    def get_subquery(self):
        return self.subquery

    def set_subquery(self, tree):
        self.subquery = tree
