from src.ResultSet.Row import Row
import src.ResultSet.ResultSetOperators.JoinStrategy as js
import src.ResultSet.ResultSetOperators.FilterStrategy as fs
import src.ResultSet.ResultSetOperators.SortStrategy as ss


class ResultSet:
    """
    Initial ResultSet class.

    TODO: should make all of these resultset functions configurable for optimizer to choose methods
    """

    def __init__(self, payload, name='N/A'):
        """
        Takes in the data from FS as a bitarray and converts to human readable format
        We store ResultSets as in a columnar way to make querying easier.
        :param payload: map[col] = [data]
        :param name: str name of resultset
        """
        self.data = payload
        self.name = name

        ###### STATS ######
        self._size = self.determine_size_rs(self.data)

    def __repr__(self):
        cell_size = 20
        header = '################### RESULTSET: {0} ###################\n'.format(self.name)
        out = ''
        for col, _ in self.data.items():
            buf = cell_size - len(col)
            out = "{0}|{1}{2}".format(out, self.get_spaces(buf), col)
        out += "|\n"
        for i in range(0, self.get_size()):
            if self._size <= i:
                break
            for col, _ in self.data.items():
                buf = cell_size - len(str(self.data[col][i]))
                out = "{0}|{1}{2}".format(out, self.get_spaces(buf), self.data[col][i])
            out += "|\n"
        return "{0}\n{1}{2}".format(self._size, header, out)

    @staticmethod
    def get_spaces(numspace):
        out = ""
        for i in range(0, numspace):
            out += " "
        return out

    ###################### JOIN ###########################
    #                                                     #
    #        logic around joins goes here                 #
    #######################################################

    def hash_join(self, right_rs, cols, j_type):
        """
        Takes a ResultSet and joins on cols provided with self.

        put everything into memory and create a big hash map on smaller DS.
        Iterate second col and join on those columns

        :param right_rs: ResultSet right side of join
        :param j_type: str join type
        :param cols: list[str] columns to join on
        :return: ResultSet output
        """
        return ResultSet(js.hash_join(self, right_rs, cols, j_type),
                         "{0}x{1}".format(self.get_name(), right_rs.get_name()))

    ###################### UTIL ###########################
    #                                                     #
    #         General RS UTIL goes here                   #
    #######################################################

    def project(self, cols):
        """
        Creates a RS based on only columns specified
        :param cols: list[str] projected columns
        :return: ResultSet new RS with correct data
        """
        if '*' in cols:
            return self
        else:
            mapping = {}
            try:
                for col in cols:
                    mapping[col] = self.data[col]
                return ResultSet(mapping, "project:{0} from {1}".format(cols, self.name))
            except KeyError as e:
                print("The projected column requested does not exist")
                raise

    @staticmethod
    def determine_size_rs(data):
        """
        Given a column from the result set, calculate and return row count

        DAN-KIM GUARANTEE: all column lists will be of the same size
        :param data: map[List]
        :return: int size of rs
        """
        return len(list(data.values())[0])

    def delete_row(self, rownum):
        """
        Deletes a row based on index
        :param rownum: int rownum
        """
        for key, val in self.data.items():
            del val[rownum]

    ###################### FILTER #########################
    #                                                     #
    #            RS Filtering goes here                   #
    #######################################################

    def filter_equal(self, col, key):
        """
        Creates new ResultSet as a subset using the key to filter
        :param col: str column to find equality on
        :param key: str, int, bool, float
        :return: ResultSet
        """
        subset = fs.equality_filter(self, col, key)
        self._size = self.determine_size_rs(self.data)
        return subset

    ###################### SORTING ########################
    #                                                     #
    #              RS Sorting goes here                   #
    #######################################################

    def order_by(self, keys):
        """
        Takes ResultSet and orders based on set of keys.
        :param keys: list[str] keys
        :return: ResultSet
        """
        return ss.sort_resultset(self, keys)

    ################ GETTERS/SETTERS ######################
    #                                                     #
    #         Getter/Setter methods placed here           #
    #######################################################

    def get_size(self):
        """
        Get size of RS (row size)
        :return: int
        """
        return self._size

    def get_data(self):
        """
        BE CAREFUL WHEN USING THIS METHOD. it does not seem like a good idea to be accessing this much memory; passing
        it around. USE WITH CAUTION. Maybe only will be used for debug purposes
        :return:
        """
        return self.data

    def get_col_data(self, key):
        """
        Given a key, return the column data from rs
        :param key: str col name
        :return: list[] col data
        """
        return self.data[key]

    def get_row(self, row_id, cols=None):
        """
        Returns a row of data based on the ROW-ID
        :param row_id: int
        :param cols: list of cols to pull specific row that follows that order
        :return: Row representing a row
        """
        new_row = Row()
        if cols:
            for col in cols:
                new_row.append(self.data[col][row_id])
        else:
            for key, _ in self.data.items():
                new_row.append(self.data[key][row_id])
        return new_row

    def get_name(self):
        return self.name

    def swap_rows(self, idx1, idx2):
        """
        given two indices, swap the rows in the resultset
        :param idx1: int
        :param idx2: int
        """
        for key, _ in self.data.items():
            temp = self.data[key][idx1]
            self.data[key][idx1] = self.data[key][idx2]
            self.data[key][idx2] = temp
