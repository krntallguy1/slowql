def equality_filter(rs, col, key):
    """
    Gets a resultset and returns a subset of it depending on if key exists.

    :param rs: ResultSet target
    :param col: str column to filter on
    :param key: str, int, bool, float => value to filter
    :return: ResultSet same resultset but subset filtered on equality filter
    """
    rs_data = rs.get_data()
    delete_list = []
    for i in range(0, len(rs_data[col])):
        if rs_data[col][i] != key:
            delete_list.append(i)
    for del_idx in delete_list:
        rs.delete_row(del_idx)
    return rs
