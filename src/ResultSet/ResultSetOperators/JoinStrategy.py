import copy

"""
This module holds all Join Strategies.
Inputs will always be ResultSets, output will always be singular ResultSet
"""


def hash_join(left_rs, right_rs, cols, j_type='inner'):
    """
    This method executes hash join logic

    This method DEFAULTS to inner join.
    TODO: implement all other types of joins later (left, right, cartesian)
    TODO: this looks memory intensive. can we optimize this?
    :param left_rs: ResultSet for left side of join
    :param right_rs: ResultSet for right side of join
    :param j_type: str join type
    :param cols: list[str] columns to join on
    :return: ResultSet output
    """
    smaller = right_rs
    larger = left_rs
    if larger.get_size() < smaller.get_size():
        temp = smaller
        smaller = larger
        larger = temp

    # create hash map first key: tuple[col_data], value: list[row]
    # guarantee that the ordering of cols must be mapped to rows
    output_cols = list(smaller.get_data().keys())
    mapping = {}
    for i in range(0, smaller.get_size()):
        # generate key based on join columns
        new_key = tuple(map(lambda x: smaller.get_data()[x][i], cols))
        # generate row based on index
        if new_key in mapping:
            mapping[new_key].append(smaller.get_row(i))
        else:
            mapping[new_key] = [smaller.get_row(i)]

    # iterate larger size and attach to currently existing row
    # first find columns in larger ds that dont belong in join
    non_joined_cols = []
    for inc_col in larger.get_data():
        if inc_col not in cols:
            non_joined_cols.append(inc_col)

    # iterate through each row, create a join key and append to mapped row
    joined_subset = {}
    for i in range(0, larger.get_size()):
        new_key = tuple(map(lambda x: larger.get_data()[x][i], cols))
        if new_key in mapping:
            temp = copy.deepcopy(mapping[new_key])
            for key in temp:
                if len(non_joined_cols) > 0:
                    new_row = larger.get_row(i, non_joined_cols)
                    key.extend(new_row)
            if new_key in joined_subset:
                joined_subset[new_key].extend(temp)
            else:
                joined_subset[new_key] = temp
    del mapping

    # append output_cols and create new RS out of the mapping
    output_map = {}
    # Create mapping beforehand with col names
    for i in range(0, len(non_joined_cols)):
        if non_joined_cols[i] in output_cols:
            non_joined_cols[i] = "{0}.{1}".format(larger.get_name(), non_joined_cols[i])
    output_cols.extend(non_joined_cols)
    for output_col in output_cols:
        output_map[output_col] = []
    # iterate through each row created and make columnar store again.
    for key, val in joined_subset.items():
        for i in range(0, len(output_cols)):
            for row in val:
                output_map[output_cols[i]].append(row.lookup_data(i))
    return output_map
