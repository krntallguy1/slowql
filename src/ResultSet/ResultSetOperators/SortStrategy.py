import random


def sort_resultset(rs, keys, strategy='quicksort'):
    """
    This algorithm takes the first key in the list and sorts the RS based on that.
    If the key is non-unique and more than one column is specified, then take the
    'lower' of the two based on second key, so on and so forth.

    By default, use a QuickSort algorithm.
    TODO: the optimizer should choose not to use quicksort if it knows it cant fit in memory

    TODO: pt 2, for now we will only sort on the FIRST KEY
    :param rs: ResultSet to sort
    :param keys: keys to sort on
    :param strategy: str sorting strategy to use
    :return: ResultSet sorted resultset
    """
    if strategy == 'quicksort':
        col_data = rs.get_col_data(keys[0])
        quicksort(rs, col_data, 0, len(col_data) - 1)
    return rs


###### QuickSort Methods #####
def partition(rs, col_data, low, high):
    random_pivot = random.randint(low, high)
    pivot = col_data[random_pivot]
    tracker = low - 1
    for i in range(low, high):
        if col_data[i] <= pivot:
            tracker += 1
            rs.swap_rows(i, tracker)
    rs.swap_rows(high, tracker + 1)
    return tracker + 1


def quicksort(rs, col_data, low, high):
    if low < high:
        new_piv = partition(rs, col_data, low, high)
        quicksort(rs, col_data, low, new_piv - 1)
        quicksort(rs, col_data, new_piv + 1, high)
