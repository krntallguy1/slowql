class Row:
    def __init__(self):
        self.row_data = []

    def append(self, data):
        """
        TODO: Make all rs and cols and stuff immuatable
        :param data: _ new data
        """
        self.row_data.append(data)

    def extend(self, data):
        """
        extends row
        :param data: row
        """
        self.row_data.extend(data.get_row_data())

    def get_row_data(self):
        """
        Getter for row data
        :return: list
        """
        return self.row_data

    def lookup_data(self, index):
        """
        does a lookup into the row based on the index
        :param index: int
        :return: _ data type depends on what is in the index
        """
        return self.row_data[index]
