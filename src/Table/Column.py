class Column:
    def __init__(self, colname, datatype):
        self.colname = colname
        self.datatype = self.set_datatype(datatype)

    def set_datatype(self, datatype):
        """
        Sets the type of data for the column
        :param datatype: str
        :return type type of the data
        """
        return {
            'bool': bool,
            'int': int,
            'float': float,
            'str': str
        }.get(datatype, None)

    def get_datatype_str(self):
        return {
            bool: 'bool',
            int: 'int',
            float: 'float',
            str: 'str'
        }.get(self.datatype, None)

    def get_col_name(self):
        return self.colname

    def get_data_type(self):
        return self.datatype

    def __str__(self):
        out = "column name: {0}, data type: {1}".format(self.colname, self.datatype)
        return out
