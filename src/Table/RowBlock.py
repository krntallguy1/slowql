import string
import struct


class StringTooLong(Exception):
    pass


class RowBlock:
    """
    This is the bit interpretation of each row.
    We define rules here for each table and convert data types to binary string that bitarray can append as blocks.
    for Strings: 8-bits PER Character (32 byte limit)
    for integers: 2^numbits (default 4 bytes) [sign, (31 bit representation)]
    for Floats: (default 40 bits) [sign, (8 bits for exponent), (32 bits for Mantissa)] truncates after 4 decimal places
    for Bool: 1 bit

    TODO: create a static representation of this in file so that we dont have to recompute
    """

    def __init__(self, columns):
        """
        datatypes => ([colnames], [data_types])
        :param columns: list[Column()]
        """
        self.datatypes = self.parse_datatypes(columns)
        self.mappings = {
            bool: 1,
            int: 32,
            float: 40,
            str: 8 * 32
        }
        self.blocksize = self.set_blocksize()

    @staticmethod
    def parse_datatypes(columns):
        """
        returns a list of datatypes that is the blocking rules
        :param columns: list[Column()]
        :return: list[type]
        """
        dtypes = []
        order = []
        for col in columns:
            dtypes.append(col.get_data_type())
            order.append(col.get_col_name())
        return order, dtypes

    def set_blocksize(self):
        """
        Sets the blocksize for the table.
        :return:
        """
        blocksize = 0
        for dtype in self.datatypes[1]:
            blocksize += self.mappings[dtype]
        return blocksize

    def return_binaryblock(self, data):
        """
        Parses through the input data and creates a block representation of the row
        :param data: map
        :return: str block in binary representation (str)
        """
        output = {}
        result = []
        for key, val in data.items():
            f = self.get_binary_rep(type(val))
            output[key] = f(val)
        for col in self.datatypes[0]:
            result.append(output[col])
        return ''.join(result)

    def get_blocksize(self):
        return self.blocksize

    def get_binary_rep(self, dtype):
        """
        Given the value and the datatype, converts to binary string
        :param val: [bool, float, int, str] input value
        :param dtype: [bool, float, int, str] input type
        :return: lambda function that converts input to binary string
        """
        return {
            int: lambda x: self.convert_int(x),
            float: lambda x: self.convert_float(x),
            str: lambda x: self.convert_string(x),
            bool: lambda x: '1' if x else '0'
        }.get(dtype, None)

    def get_pydata_from_binary(self, dtype):
        """
        complimentary method to get_binary_rep
        takes bits and returns pythonic data rep
        :param dtype: type
        :return: int | str | float | bool
        """
        return {
            int: lambda x: self.convert_bits_to_int(x),
            float: lambda x: self.convert_bits_to_float(x),
            str: lambda x: self.convert_bits_to_str(x),
            bool: lambda x: self.convert_bits_to_bool(x),
        }.get(dtype, None)

    def convert_bits_to_float(self, bits):
        """
        Takes binary and converts to python float
        :param bits: list of bits
        :return: float
        """
        exp = int(bits[1:9].to01(), 2)
        mantissa = int(bits[9:].to01(), 2)
        if exp == 0:
            exp = 1
        else:
            exp = -exp
        data = float(pow(mantissa, exp))
        if bits[0]:
            return -data
        else:
            return data

    def convert_bits_to_int(self, bits):
        """
        Takes binary and converts to python int
        :param bits: list of bits
        :return: int
        """
        return int(bits.to01(), 2)

    def convert_bits_to_bool(self, bits):
        """
        Takes binary and converts to python bool
        :param bits: bit
        :return: bool
        """
        return bool(bits)

    def convert_bits_to_str(self, bits):
        """
        Takes binary and converts back to python string
        :param bits: list of bits
        :return: str
        """
        return str(bits.tobytes().decode('utf-8')).replace('\x00', '')

    @staticmethod
    def convert_float(data):
        """
        Takes a float and converts to 32 bit string representation
        :param data: float
        :return: str 32 bit representation
        """
        negative = False
        if data < 0:
            negative = True
            data = -data
        small = 0.00000001
        data = int(data * 10000) / 10000.0
        exp = 0
        while data % 1 > small:
            c = data % 1
            data = data * 10
            exp += 1
        int_rep = int(data)
        mantissa = '{0:031b}'.format(int_rep)
        exponent = '{0:08b}'.format(exp)
        if negative:
            return '1{0}{1}'.format(exponent, mantissa)
        else:
            return '0{0}{1}'.format(exponent, mantissa)

    @staticmethod
    def convert_int(data):
        """
        Takes an integer and converts to 32 bit string representation
        :param data: int
        :return: str 32 bit representation
        """
        if data < 0:
            data = -data
            return '1{0:031b}'.format(data)
        return '{0:032b}'.format(data)

    def convert_string(self, data):
        """
        Takes a string and converts into bytes
        TODO: use encode decode in bitarray lib
        :param data: str
        :return: str 32 bytes represented
        """
        if len(data) > 32:
            raise StringTooLong("This String inputted is too long")
        arr = bytearray(data, 'utf-8')
        bin_arr = []
        for char in arr:
            bin_arr.append('{0:08b}'.format(char))
        result = ''.join(bin_arr)
        return result.rjust(self.mappings[str], '0')

    def bits_to_data(self, bitmapping):
        """
        Makes the conversion of the data held as bitarray to human readable data
        :param bitmapping: bitarray
        :return: map[column] = col_data
        """
        output = {}
        extra = len(bitmapping) % self.get_blocksize()
        colnames = self.datatypes[0]
        dtypes = self.datatypes[1]
        intermediate = []
        idx = 0
        for col in colnames:
            output[col] = []
            intermediate.append((col, dtypes[idx]))
            idx += 1
        counter = 0
        while counter < len(bitmapping) - extra:
            for d in intermediate:
                offset = self.mappings[d[1]]
                bits = bitmapping[counter:counter + offset]
                data = self.get_pydata_from_binary(d[1])(bits)
                output[d[0]].append(data)
                counter += offset
        return output
