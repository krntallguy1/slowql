from bitarray import bitarray
from src.Table.RowBlock import RowBlock


class InvalidColumnStructure(Exception):
    pass


class ColumnNameNotFoundException(Exception):
    pass


class ColumnMismatchException(Exception):
    pass


class Table:
    def __init__(self, tablename, columndata):
        """
        :param tablename: str tablename
        :param columndata: list[Column()] columndata
        """
        self._tablename = tablename
        # ordered list of Column type data.
        # see Column.py for definition
        self.column = columndata
        self.rowblock = RowBlock(self.column)
        self.data = bitarray()

    def insert_row(self, data):
        """
        Tries to insert a row of data based on user input.
        If there is a column mismatch, throw error.
        :param data: map row data
        """
        data_in = self.parse_data(data)
        bin_row = self.rowblock.return_binaryblock(data_in)
        self.data.extend(bin_row)

    def get_tablename(self):
        """
        return table name
        :return: string tablename
        """
        return self._tablename

    def get_colummn_info(self):
        """
        returns column defs
        :return: dict of column defs
        """
        return list(map(lambda x: (x.get_col_name(), x.get_datatype_str()), self.column))

    def parse_data(self, data):
        """
        Insert Into command will default to:

        slowql> insert into DANIEL (14, kim, True, 0.00)

        where Schema of Daniel is:
        (age int, surname str, employed bool, gpa float)

        :param data: str data provided by user
        :return: map data in proper data format
        """
        idx = 0
        data_out = {}
        try:
            for col in data.split(','):
                col = col.strip()
                col_info = self.column[idx]
                dtype = col_info.get_data_type()
                data_out[col_info.get_col_name()] = dtype(col)
                idx += 1
            if idx - 1 < len(self.column) - 1 or idx > len(self.column):
                raise ColumnMismatchException("Number of inputs did not match output")
            else:
                return data_out
        except (ValueError, IndexError):
            raise

    def get_data(self):
        """
        returns bitmapping of the data held
        :return bitarray
        """
        return self.data

    def set_data(self, data):
        """
        Sets the bitmapping to table
        :param data: bitarray
        """
        self.data = data

    def get_pydata(self):
        """
        Data for scans
        :return: map[colname] = [data]
        """
        return self.rowblock.bits_to_data(self.data)

    def __repr__(self):
        out = "tablename: {0}, columns: {1}".format(self._tablename, self.get_colummn_info())
        return out
