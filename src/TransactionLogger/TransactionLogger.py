import time


class TransactionLogger:
    """
    Transaction logging class. Will store transaction logs up to certain buffer.
    """

    def __init__(self, tlogpath):
        # list of transaction
        self.buffer = []
        self.tlogpath = tlogpath

    def dump_logs(self):
        """
        When called, should dump to appropriate file sequentially
        """
        for log in self.buffer:
            with open(self.tlogpath, 'a') as logfile:
                logfile.write(log)
        self.buffer = []
        return

    def add_to_buffer(self, command):
        """
        adds a transaction to be staged in the buffer.
        :param command: the regex command to be staged
        """
        self.buffer.append(command)

    def print_buffer(self):
        """
        dumps buffer in printable format for user to see
        """
        print(self.buffer)

    def add_log_create_table(self, tablename, columns):
        """
        Add a create table log
        :param tablename: str name of table
        :param columns: list col definitions
        """
        self.add_to_buffer(('create_table', time.time(), tablename, columns))

    def add_log_insert_into(self, tablename, data):
        """
        Add a insert into log
        :param tablename: str name of table
        :param data: list data
        """
        self.add_to_buffer(('insert_into', time.time(), tablename, data))
