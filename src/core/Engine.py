import pprint

_version = '0.1'


class QueryEngine:
    """
    Main Query Engine class

    handles logical computation of SQL
    handles DML and DDL as well.

    TODO: Create queue system here that follows ATOM principles.
    """

    def __init__(self, fs):
        self.FS = fs
        self.pp = pprint.PrettyPrinter(indent=4)

    def execute(self, executiontree):
        """
        should handle execution by running the head of the tree itself.
        :param executiontree: ExecutionNode head of execution tree
        :return: resultset
        """
        return executiontree.execute()
