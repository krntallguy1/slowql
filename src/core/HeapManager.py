import resource
import sys


class HeapManager:
    """
    This is the heap manager for slowql.
    This manager has the task of preventing slowql from going oom.

    TODO: THIS CLASS MUST BE THREAD SAFE. All threads will share this info.

    General Strategy:
    1) Check how much addressable memory exists on the system
    2) Set default to 60% MAX capacity => Set as total volume of space (VOL)
    3) breakdown of VOL:
        a) BufferPool (holds 50% of VOL)
        b) WorkSpace (holds 50% of VOL)

    These numbers are experimental. Will play around with them
    """

    def __init__(self, config):
        self.platform = sys.platform
        self.sql_config = config
        total_heap_size = self.sql_config.get_conf_value('MaxHeapSize')
        self.set_heap_limit(total_heap_size)
        heap_to_buffer_ratio = .5
        heap_to_workspace_ratio = .5
        self.object_overhead = self.sql_config.get_conf_value('SlowQLOverhead')
        self.usable_heap = total_heap_size - self.object_overhead

        ### KEY HEAP USERS ###
        self._usable_workspace = self.usable_heap * heap_to_workspace_ratio
        self._bufferpool_space = self.usable_heap * heap_to_buffer_ratio

    @staticmethod
    def get_resource_usage_report():
        r = resource.getrusage(resource.RUSAGE_SELF)

        key_to_description = {
            "ru_utime": "time in user mode (float)",
            "ru_stime": "time in system mode (float)",
            "ru_maxrss": "maximum resident set size",
            "ru_ixrss": "shared memory size",
            "ru_idrss": "unshared memory size",
            "ru_isrss": "unshared stack size",
            "ru_minflt": "page faults not requiring I/O",
            "ru_majflt": "page faults requiring I/O",
            "ru_nswap": "number of swap outs",
            "ru_inblock": "block input operations",
            "ru_oublock": "block output operations",
            "ru_msgsnd": "messages sent",
            "ru_msgrcv": "messages received",
            "ru_nsignals": "signals received",
            "ru_nvcsw": "voluntary context switches",
            "ru_nivcsw": "involuntary context switches",
        }

        return dict([(v, getattr(r, k)) for k, v in key_to_description.items()])

    @staticmethod
    def set_heap_limit(size):
        """
        Sets the heap size limit of SlowQL
        :param size: size of max heap in bytes
        """
        resource.setrlimit(resource.RLIMIT_RSS, (size, size))

    def request_workspace_heap(self, size):
        """
        Makes a request to heapmanager for space on Workspace side
        :param size: int size of request in bytes
        :return: True if granted, False if not.
        """
        if self._usable_workspace > size:
            self._usable_workspace -= size
            return True
        else:
            return False

    def request_bufferpool_heap(self, size):
        """
        Makes a request to heapmanager for space on BufferPool side
        :param size: int size of request in bytes
        :return: True if granted, False if not
        """
        if self._bufferpool_space > size:
            self._bufferpool_space -= size
            return True
        else:
            return False
