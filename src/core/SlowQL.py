from src.core.SlowQLInterpreter import *
from src.core.SlowQLFS import SlowQLFileSys
from src.core.Engine import *
from src.ExecutionBuilder.ExecutionBuilder import *
from src.core.HeapManager import HeapManager
from src.core.SlowQLConf import SQLConf
from src.ExecutionBuilder.TableScan import TableNotFoundException
from src.BufferPool.BufferPool import BufferPool

import logging
import time
import traceback
from concurrent.futures import ThreadPoolExecutor
# from concurrent.futures import ProcessPoolExecutor


class SlowQL:
    """
    Singleton to dynamically take SQL input, compile, and run SlowQL.

    using 'python SlowQL.py' for interactive usage
    """

    _instance = None

    @staticmethod
    def get_instance():
        if SlowQL._instance is None:
            SlowQL()
        return SlowQL._instance

    def __init__(self, fsDir='/tmp/Data/slowql'):
        # singleton check
        if SlowQL._instance is not None:
            raise Exception("This module is to be used as a singleton. Please pass reference around")
        else:
            SlowQL._instance = self

        # will initially store data in ~/Data/slowql/
        # Initialize configuration file
        self.config = SQLConf()

        self.heap_manager = HeapManager(self.config)
        self.fsDir = fsDir

        # logger
        logging.basicConfig(filename='./Logs/slowql.log',
                            filemode='w',
                            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.DEBUG)

        self.logger = logging.getLogger(__name__)
        self.interpret = SlowQLInterpreter()
        self.BP = BufferPool()
        self.FS = SlowQLFileSys(self.fsDir, self.BP)
        self.queryEngine = QueryEngine(self.FS)
        # Set default thread limit to 4 for now.
        # self.executor = ProcessPoolExecutor(max_workers=4)
        self.executor = ThreadPoolExecutor(max_workers=4)

    def runSQL(self, sql):
        """
        Generic Run SQL command
        :param sql: str user input
        :return: resultset
        """
        self.logger.info("SQL to be run: %s", sql)
        valid_exp = self.interpret.check_input(sql)
        expression_tree = ExecutionBuilder(valid_exp, self.FS)
        rs = self.queryEngine.execute(expression_tree.build())
        return rs

    def shutdown(self):
        self.FS.dump()

    def reset(self):
        self.FS.reset_fs()

    def enqueue(self, sql):
        """
        Sends SQL as a thread to the thread pool. Thread pool will manage concurrency for us.
        :param sql: str
        :return Future
        """
        result = self.executor.submit(self.runSQL, sql)
        return result.result()


if __name__ == '__main__':
    d = SlowQL()
    sql_input = ''
    while sql_input != 'exit':
        sql_input = input("slowql> ")
        if sql_input == 'reset':
            d.reset()
        elif sql_input == 'exit':
            continue
        else:
            try:
                print(d.enqueue(sql_input))
            except (IncorrectSyntaxException, InputTypeException, IncompleteSQLException, TableNotFoundException,
                    ValueError, Exception):
                traceback.print_exc()
                d.shutdown()
    d.shutdown()
