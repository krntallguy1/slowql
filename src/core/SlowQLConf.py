import os


class SQLConf:
    """
    This class represents the configs for SlowQL.
    Read from ../../conf/slowql.conf

    If configs need to be changed, can manually replace in that file.

    """
    def __init__(self):
        curr_file_path = os.path.dirname(__file__)
        conf_file_path = os.path.join(curr_file_path, '../../conf/slowql.conf')
        self.mapping = {}
        self.read_conf(conf_file_path)

    def read_conf(self, file_path):
        f = open(file_path)
        for line in f:
            pair = line.strip().split('=')
            self.mapping[pair[0].strip()] = self.convert_to_bytes(pair[1].strip())
        f.close()

    def get_conf_value(self, key):
        """
        Gets a config value from mapping. If it does not exist,
        returns None
        :param key: str key value
        :return:
        """
        if key in self.mapping:
            return self.mapping[key]
        else:
            return None

    @staticmethod
    def convert_to_bytes(val):
        val = val.lower()
        base = 1024
        if 'gb' in val:
            return int(val.split('gb')[0]) * pow(base, 3)
        elif 'mb' in val:
            return int(val.split('mb')[0]) * pow(base, 2)
        elif 'kb' in val:
            return int(val.split('kb')[0]) * base
        else:
            return int(val)
