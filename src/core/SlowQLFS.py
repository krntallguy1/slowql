import os
import json
import errno
import shutil
from bitarray import bitarray

from src.Table.Table import Table
from src.Table.Column import Column
from src.TransactionLogger.TransactionLogger import *
from src.BufferPool.BufferPool import TableFailedtoAddException, TableNotFoundException


class CorruptedFSException(Exception):
    pass


class TableExistsException(Exception):
    pass


_version = '0.1'


class SlowQLFileSys:
    """
    This is the first rudimentary in-memory file system I am ATTEMPTING to create.
    Will definitely reiterate this class over and over to get more optimized designs.

    ON DISK HIERARCHY:
    | tmp
    | ==> Data
    |    | ==> slowql
    |           | ==> md
    |           | ==> tLogs
    |                 | ==> SlowQL-history.log
    |           | ==> tsData
    |                 | ==> aux
    |                     | ==> INDEX
    |                 | ==> tsmd
    |                 | ==> tablename0.DATA

    Inital design:
    Make in-memory DB, with a transaction log to persist data on disk.
    This class serves as the in-memory file sys

    Notes:
    1) How to store metadata on disk to be read in memory.
    2) If data persisted previously, how to dump into memory to query.
    3) File object store for efficiency?
    """

    def __init__(self, fsDir, BufferPool):
        ########### VARIABLES ###########
        #        FileSystem related     #
        #   variables stored on start   #
        #################################
        self.metadatafilename = "md"
        self.transactionlogname = "SlowQL-history.log"
        self.transactionloggingdir = "tLogs"
        self.tablespacedir = "tsData"
        self.fsDir = fsDir
        self.metadata = None
        self.DSMap = None
        self.filedir = None

        ##### IN MEMORY BUFFER POOL #####
        #      This class handles BP    #
        #    Might refactor this out    #
        #################################
        self.BP = BufferPool

        ######### ON START UP ###########
        #        Start up functions     #
        #  Must occur before operating  #
        #################################

        self.tloggingdir = os.path.join(self.fsDir, self.transactionloggingdir)
        self.tloggingpath = os.path.join(self.tloggingdir, self.transactionlogname)
        self.metadatafilepath = os.path.join(self.fsDir, self.metadatafilename)
        self.tablespacepath = os.path.join(self.fsDir, self.tablespacedir)
        self.onStart()

        ###### TRANSACTION LOGGER  ######
        #      This class handles BP    #
        #    Might refactor this out    #
        #################################

        self.transactionlogger = TransactionLogger(self.tloggingpath)

    def onStart(self):
        """
        start up functionality. Can call if needed to restart.
        Does Filesystem health check as well.
        """
        # Creating spaces, files, directories
        self.init_base_dir()
        self.create_metadata_file()
        self.init_tablespaces()
        self.init_tlogging_space()
        self.create_tlog_file()

        # grabbing necessary metadata
        md = self.get_catalog_metadata()
        self.DSMap = md["DSMap"]
        self.filedir = md["filedir"]
        self.repopulate()

        # Filesystem health check
        self.fs_healthcheck()

    def init_base_dir(self):
        """
        creates base dir: self.fsDir
        :return str name of base dir
        """
        try:
            os.makedirs(self.fsDir)
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
        return self.fsDir

    def fs_healthcheck(self):
        """
        Checks overall health of file system. Makes sure we are within healthy boundaries for storage
        1) checks existence of necessary metadata files
        2) Ensures RAM usage, Disk Space is not being violated
        3) looks for corrupted data (?)
        :return: 200: green, 300s: warn, 500: danger
        """

        # initial entry check for disk
        # This will safeguard against outside user breaches.
        if not os.path.exists(self.fsDir):
            raise CorruptedFSException
        if not os.path.exists(self.tablespacepath):
            raise CorruptedFSException
        if not os.path.exists(self.tloggingpath):
            raise CorruptedFSException
        return 200

    def create_tlog_file(self):
        self.checkandcreatedir(self.fsDir)
        with open(self.tloggingpath, "w+") as outfile:
            outfile.write("START OF HISTORY")
            outfile.write("=================================================")

    def create_metadata_file(self):
        """
        Creates meta data file on start up
        """
        if not os.path.exists(self.metadatafilepath):
            with open(self.metadatafilepath, "w+") as outfile:
                data = {"DSMap": {},
                        "filedir": {}}
                json.dump(data, outfile)

    def get_catalog_metadata(self):
        """
        Grabs meta data from static route under: ~/data/slowql/md
        :returns data: json blob
        """
        if not os.path.exists(self.metadatafilepath):
            # TODO: Mechanism to be fail safe if metadata file is missing.
            raise CorruptedFSException("metadata file missing or corrupted. Will try to generate new one")
        else:
            with open(self.metadatafilepath) as meta_json:
                data = json.load(meta_json)
                return data

    def init_tablespaces(self):
        """
        If tablespaces are not created, init the directory.
        :return: fullpath to tablespace
        """
        self.checkandcreatedir(self.tablespacepath)
        return self.tablespacepath

    def init_tlogging_space(self):
        self.checkandcreatedir(self.tloggingdir)
        return self.tloggingdir

    ######################### IN MEMORY FUNCTIONS ###########################
    #         executing queries and DDL/DML on demand in-memory             #
    #         handle offloading onto disk when CPU/Memory is abundant       #
    #########################################################################
    def repopulate(self):
        """
        Gets called to load up all tables back into memory
        Create tables and place back into buffer pool
        """
        for key, val in self.filedir.items():
            data = bitarray()
            with open(self.filedir[key], 'rb') as f:
                data.fromfile(f)
            coldefs = self.DSMap[key]
            colli = []
            for col in coldefs:
                colli.append(Column(col[0], col[1]))
            reopened = Table(key, colli)
            self.BP.add_table(reopened)
            reopened.set_data(data)

    def create_table(self, tablename, columns):
        """
        Create new table with options
        For now, we will only accept "Create Table ______(options)"
        TODO: make it possible to copy table definitions
        :param tablename: str name of table
        :param columns: list of column types
        :return: returncode 200 if pass, 500 if failed.
        """
        if tablename in self.DSMap:
            raise TableExistsException("The table: {0} already exists in Tablespaces. Try different name"
                                       .format(tablename))
        else:
            try:
                tab = Table(tablename, columns)
                self.BP.add_table(tab)
                self.DSMap[tablename] = tab.get_colummn_info()
            except TableFailedtoAddException:
                return 500
        self.create_tablespace(tablename)
        self.transactionlogger.add_log_create_table(tablename, columns)
        return tab

    def get_table(self, tablename):
        """
        Grab table from in-memory BufferPool.
        If it cannot be found in BP, try to retrieve from disk
        :param tablename: name of table to return
        :return: table
        """
        try:
            if tablename in self.DSMap:
                return self.BP.get_table(tablename)
        except TableNotFoundException:
            # TODO: disk table retrieval.
            raise

    def scan(self, tablename):
        """
        Complimentary TableScan method for FS
        :param tablename: str name of table
        :return: tuple (bitarray, list[Column()]): data, column information
        """
        tab = self.get_table(tablename)
        return tab.get_pydata()

    def insert_row(self, tablename, data):
        """
        Inserts a row of the table in the FS
        :param tablename: str name of table
        :param data: data
        """
        tab = self.get_table(tablename)
        tab.insert_row(data)
        self.transactionlogger.add_log_insert_into(tablename, data)
        return

    def show_tables(self):
        """
        grabs FS table metadata and returns to user.
        TODO: MUST MAKE SURE THIS PROCESS IS ATOMIC.
        :return:
        """
        return self.DSMap

    ####################### TRANSACTIONAL FUNCTIONS #########################
    #         Create transactional handlers. use SlowQLFS in down times     #
    #         handle offloading onto disk when CPU/Memory is abundant       #
    #########################################################################

    def create_tablespace(self, tablename):
        """
        Each new table will create a new tablespace.

        Questions for future Dan:
        1) should we partition the table
            - If we leave in one large file, will it be too slow to load into memory?
        2) how big should the file be?
            - This relates to question 1

        TODO: Hash out the tablename directory so its not easy to see.

        :param tablename: name of the table
        :return: full path to the directory of the table
        """
        # create tablespace dir
        tbs = os.path.join(self.tablespacepath, tablename)

        # aux dir: holds index info and meta data
        aux = os.path.join(tbs, 'aux')

        # tablespace metadata
        md = os.path.join(aux, 'tsmd')

        # actual data stored here.
        data = os.path.join(tbs, tablename + '0.DATA')
        self.filedir[tablename] = data

        # create dirs
        self.checkandcreatedir(tbs)
        self.checkandcreatedir(aux)

        # create files
        with open(md, 'w+') as newfile:
            newfile.write('tablespace metadata')
        with open(data, 'w+') as datafile:
            pass

    def _delete_tablespace(self, tablename):
        """
        INTERNAL USE for testing (as of now)
        Deletes tablespace and all associated data.

        MAKE SURE YOU KNOW WHAT YOU'RE DOING BEFORE CALLING THIS.

        :param tablename: str name of table
        """
        tbs = os.path.join(self.tablespacepath, tablename)
        shutil.rmtree(tbs)
        if os.path.exists(tbs):
            raise CorruptedFSException("Something in the FS went horribly wrong")
        else:
            return tablename

    def _delete_fs(self):
        """
        INTERNAL USE for testing (as of now)
        Deletes all related data. Fresh start.
        """
        shutil.rmtree(self.fsDir)
        if os.path.exists(self.fsDir):
            raise CorruptedFSException("Something in the FS went horribly wrong")
        else:
            return

    def reset_fs(self):
        """
        resets FS to clean out-of-box state
        """
        self._delete_fs()
        self.BP.reset()
        self.__init__(self.fsDir, self.BP)

    def dump(self):
        """
        dumps the DS Map into static file. Call once in a while.
        Always should be called on db close.
        """
        with open(self.metadatafilepath, 'w+') as newfile:
            data = {"DSMap": self.DSMap,
                    "filedir": self.filedir}
            json.dump(data, newfile)
        self.flush_bits_to_file()

    def flush_bits_to_file(self):
        """
        flush all tables to disk
        """
        for key, val in self.filedir.items():
            with open(self.filedir[key], 'wb') as f:
                self.BP.get_table(key).get_data().tofile(f)

    ################################# UTILITY ###############################
    #                                                                       #
    #                             Generic Utilities                         #
    #########################################################################

    @staticmethod
    def checkandcreatedir(dirname):
        if not os.path.exists(dirname):
            try:
                os.makedirs(dirname)
                return False
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise
        else:
            return True
