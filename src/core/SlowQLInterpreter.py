from src.ParserExpressions.ActiveExpressions.Select_Statement import *
from src.ParserExpressions.ActiveExpressions.From_Statement import *
from src.ParserExpressions.ActiveExpressions.Create_Table_Statement import *
from src.ParserExpressions.StaticExpressions.Static_Expressions import *
from src.ParserExpressions.ActiveExpressions.Join_Statement import *
from src.ParserExpressions.ActiveExpressions.On_Statement import *
from src.ParserExpressions.ActiveExpressions.Where_Statement import *
from src.ParserExpressions.ActiveExpressions.Show_Tables_Statement import *
from src.ParserExpressions.ActiveExpressions.Insert_Into_Statement import *
from src.ParserExpressions.ActiveExpressions.Operator_Statement import *
from src.ParserExpressions.StaticExpressions.SubQuery import SubQuery
from src.ParserExpressions.ActiveExpressions.Order_By_Statement import *

import logging

_version = '0.1'


class InputTypeException(Exception):
    pass


class IncorrectSyntaxException(Exception):
    pass


class IncompleteSQLException(Exception):
    pass


class SlowQLInterpreter:
    """
    Interpreter class that handles parsing/creating expression tree.
    While building expression tree, this class also validates.

    Each interpreter should handle their own layer of subqueries.
    """

    def __init__(self):
        self._version = '0.1'
        self.logger = logging.getLogger(__name__)
        self.subqueries = {}
        self.active_exp = {
            'select': 'select',
            'from': 'from',
            'where': 'where',
            'create_table': 'create table',
            'insert_into': 'insert into',
            'show_tables': 'show tables',
            'inner_join': 'inner join',
            'outer_join': 'outer join',
            'order_by': 'order by',
            'full_join': 'full join',
            'on': 'on',
            '>': 'operator',
            '<': 'operator',
            '=': 'operator',
            '<=': 'operator',
            '>=': 'operator'
        }

    def check_input(self, inputstring):
        """
        Initial error checking needed for input string specified.
        :param inputString:
        :return:
        """
        if type(inputstring).__name__ != 'str':
            raise InputTypeException("WHAT ARE YOU DOING. MUST BE STRING INPUT")
        else:
            inputstring = self.find_subqueries(inputstring)
            return self.input_match(inputstring)

    def input_match(self, input):
        """
        matches input and creates active and static expressions nodes.

        :param input: input string to check
        :return: expression head of valid expression
        """
        input = self.clean_active_expression(input.split())
        input = self.clean_static_expression(input)
        expression_head = self.identify_expressions(input)
        if self.validate_chain(expression_head):
            self.logger.info('Successfully validated SQL')
            return expression_head
        else:
            raise IncorrectSyntaxException("SQL is formatted incorrectly")

    def validate_chain(self, expression_head):
        """
        starts from beginning of expression chain and validates each active expression
        node.
        :param expression_head: expression node start of node
        :return: True if valid
        """
        head = expression_head
        next = head.get_next()
        try:
            while head is not None:
                if isinstance(head, Active_Expression):
                    head.validate(next)
                # messy, but need to validate the None case at end.
                if next is None:
                    break
                head = next
                next = next.get_next()
            head = expression_head

            head.validated = True
            # case to cover extra nodes
            while head is not None:
                if not head.validated:
                    return False
                head = head.get_next()
            return True
        except InterpretError:
            raise

    def identify_expressions(self, input):
        """
        Recursively traverses input and builds expression chain while verifying structure if SQL.
        :param input: list input delimited by spaces
        :return: current expression node
        """
        currexp = self.get_active_exp(input[0].lower())
        input.pop(0)
        if isinstance(currexp, SubQuery):
            if 'subq' in currexp.get_input():
                currexp.set_subquery(self.subqueries[currexp.get_input()])
        if len(input) == 0:
            return currexp
        else:
            return currexp.append(self.identify_expressions(input))

    def clean_active_expression(self, input):
        """
        cleans up input list so that we combine clauses/expressions that are split by space and consolidates into
        one nice list of expressions
        :param input: str sql split string
        :return: list of nicely formatted expressions
        """
        delete_list = []
        for index in range(len(input) - 1):
            check_active = "{0}_{1}".format(input[index].lower(), input[index + 1].lower())
            if check_active in self.active_exp:
                input[index] = check_active
                delete_list.append(index + 1)
        for delete_idx in reversed(delete_list):
            input.pop(delete_idx)
        return input

    def find_subqueries(self, input):
        """
        Goes through input string and looks for FIRST outer layer of subqueries.
        for example,
        select * from daniel
            where age >
                [select age from ashley
                    where name =
                        [select name from jim]]
            and name =
                [select * from calvin]

        in this case, [select age from ashley where name = __]
        and [select * from calvin] would be handled in this layer of the interpreter

        [select name from jim] would be handled by [select age from ashley]'s
        interpreter.
        :param input: str input string
        :return: str cleaned up input string
        """
        # iterate through each character,
        output = ''
        subquery = ''
        found_sub = False
        open_sub = 0
        subq = 0
        for i in range(0, len(input)):
            if input[i] == '[' and not found_sub:
                found_sub = True
                continue
            if found_sub:
                if input[i] == '[':
                    open_sub += 1
                elif input[i] == ']':
                    if open_sub > 0:
                        open_sub -= 1
                    else:
                        identifier = 'subq_{0}'.format(subq)
                        self.subqueries[identifier] = SlowQLInterpreter().check_input(subquery)
                        output += identifier
                        subq += 1
                        found_sub = False
                        subquery = ''
                        continue
                subquery += input[i]
            else:
                output += input[i]
        return output

    def get_subqueries(self):
        return self.subqueries

    def clean_static_expression(self, input):
        """
        cleans up input list even further by consolidating static expressions down to one input value.
        ALWAYS RUN AFTER RUNNING self.clean_active_expressions
        :param input: str sql split string
        :return: list of nicely formatted expressions
        """
        delete_list = []
        appending = []
        index = 0
        start = -1
        while index != len(input):
            if input[index].lower() not in self.active_exp:
                if start == -1:
                    start = index
                else:
                    appending.append(index)
                    delete_list.append(index)
            else:
                for app in appending:
                    input[start] = "{0} {1}".format(input[start], input[app])
                appending = []
                start = -1
            index += 1
        if len(appending) != 0:
            for app in appending:
                input[start] = "{0} {1}".format(input[start], input[app])
        for delete_idx in reversed(delete_list):
            input.pop(delete_idx)
        return input

    @staticmethod
    def get_active_exp(input):
        """
        acts as switch statement for what type of expression was received. Add onto new active expressions here
        when wanting to implement more.
        :param input: parsed expression
        :return: new expression to append to chain
        """
        if 'subq' in input:
            return SubQuery(input)
        return {
            'create_table': Create_Table(),
            'from': From(),
            'full_join': Join('full'),
            'inner_join': Join('inner'),
            'insert_into': Insert_Into(),
            'on': On(),
            'outer_join': Join('outer'),
            'order_by': Order_By(),
            'select': Select(),
            'show_tables': Show_Tables(),
            'where': Where(),
            '>': Operator('>'),
            '<': Operator('<'),
            '=': Operator('=')
        }.get(input, Static_Expression(input))
