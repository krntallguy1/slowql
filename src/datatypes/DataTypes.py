from enum import Enum


class DataTypes(Enum):
    INTEGER = 'int'
    STRING = 'str'
    BOOLEAN = 'bool'
    DOUBLE = 'float'

    @classmethod
    def has_value(cls, value):
        return any(value == item.value for item in cls)
