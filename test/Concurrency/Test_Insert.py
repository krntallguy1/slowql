import unittest
import random
from time import time
from src.core.SlowQL import *


class Test_Insert(unittest.TestCase):

    d = SlowQL.get_instance()
    random.seed(1)
    d.runSQL('Create table Daniel(name str, age int)')
    d.runSQL('Create table Jim(name str, age int)')

    def test_1000_concurrent_inserts_dan(self):
        start_time = time.time()
        for i in range(0, 1000):
            self.d.enqueue('insert into Daniel(yes, {0})'.format(i))
        end_time = time.time()
        print('time for inserts', end_time - start_time)
        start_time = time.time()
        self.d.enqueue('select * from Daniel order by age')
        end_time = time.time()
        print('time for sort', end_time - start_time)

    def test_1000_concurrent_inserts_jim(self):
        start_time = time.time()
        for i in range(0, 1000):
            self.d.enqueue('insert into Jim(yes, {0})'.format(i))
        end_time = time.time()
        print('time for inserts', end_time - start_time)
        start_time = time.time()
        self.d.enqueue('select * from Jim order by age')
        end_time = time.time()
        print('time for sort', end_time - start_time)

    def test_hash_join(self):
        start_time = time.time()
        end_time = time.time()
        print(self.d.enqueue('select * from Daniel inner join Jim on age'))
        print('time for join', end_time - start_time)
