import unittest
from src.core.SlowQLFS import *
from src.BufferPool.BufferPool import *

from src.ExecutionBuilder.CreateTable import *


class Test_CreateTableExecution(unittest.TestCase):

    def setUp(self):
        fsdir = "/tmp/test/slowql/"
        bp = BufferPool()
        self.fs = SlowQLFileSys(fsdir, bp)
        self.createtablenode = CreateTable('randomname', 'name str, age int, true? bool, salary float', self.fs)

    def tearDown(self):
        self.fs._delete_fs()

    def test_execute_1(self):
        result = self.createtablenode.execute()
        self.assertEqual(result.get_tablename(), 'randomname')

    def test_same_name_tables_fail(self):
        self.createtablenode.execute()
        with self.assertRaises(TableExistsException):
            CreateTable('randomname', 'name str', self.fs).execute()


if __name__ == '__main__':
    unittest.main()
