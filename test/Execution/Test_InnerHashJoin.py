import unittest
from src.ResultSet.ResultSet import ResultSet, Row


class Test_InnerHashJoin(unittest.TestCase):

    def setUp(self):
        daniel_data = {'name': ['daniel', 'chang', 'bum', 'kim'], 'age': [12, 13, 41, 51]}
        ashley_data = {'name': ['ashley', 'sung', 'eun', 'kim', 'kim'], 'age': [12, 13, 41, 51, 21]}

        jim_data = {'col1': ['a', 'c', 'c'], 'col2': [99, 23, 10]}
        cat_data = {'col1': ['a', 'b', 'c', 'd', 'd'], 'col2': [14, 5, 123, 44, 2]}

        self.dan_rs = ResultSet(daniel_data, 'dan')
        self.ashley_rs = ResultSet(ashley_data, 'ashley')
        self.jim_rs = ResultSet(jim_data, 'jim')
        self.cat_rs = ResultSet(cat_data, 'cat')

    def tearDown(self):
        pass

    def test_execute_1(self):
        """
        Execute a join on name only. Only key that should be joined is `kim`
        Also, this is a case where the probe side has two of same key
        """
        join_cols = ['name']
        rs = self.dan_rs.hash_join(self.ashley_rs, join_cols, 'inner')
        self.assertEqual(2, rs.get_size())
        self.assertEqual({'name': ['kim', 'kim'], 'ashley.age': [51, 21], 'age': [51, 51]}, rs.get_data())
        self.assertEqual(['kim', 51, 51], rs.get_row(0).get_row_data())
        self.assertEqual(['kim', 51, 21], rs.get_row(1).get_row_data())

    def test_execute_2(self):
        """
        Execute a join with multiple join columns
        """
        join_cols = ['name', 'age']
        rs = self.dan_rs.hash_join(self.ashley_rs, join_cols, 'inner')
        self.assertEqual(1, rs.get_size())
        self.assertEqual({'name': ['kim'], 'age': [51]}, rs.get_data())

    def test_execute_3(self):
        """
        Execute a join and project different column
        """
        join_cols = ['name']
        rs = self.dan_rs.hash_join(self.ashley_rs, join_cols, 'inner')
        proj_rs = rs.project(['age', 'ashley.age'])
        self.assertEqual({'age': [51, 51], 'ashley.age': [51, 21]}, proj_rs.get_data())

    def test_execute_4(self):
        """
        Execute a join where the hash side has two of same key
        """
        join_cols = ['col1']
        rs = self.jim_rs.hash_join(self.cat_rs, join_cols, 'inner')
        self.assertEqual({'cat.col2': [14, 123, 123], 'col1': ['a', 'c', 'c'], 'col2': [99, 23, 10]}, rs.get_data())
