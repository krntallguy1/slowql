import unittest
from os.path import expanduser

from src.core.SlowQLFS import *
from src.BufferPool.BufferPool import *

from src.ExecutionBuilder.CreateTable import *


class Test_FileSystem(unittest.TestCase):

    def setUp(self):
        home = expanduser("~")
        self.fsDir = os.path.join(home, "/tmp/test/slowql")
        self.fs = SlowQLFileSys(self.fsDir, BufferPool())

    def tearDown(self):
        self.fs._delete_fs()

    def test_setup(self):
        self.assertEqual(200, self.fs.fs_healthcheck())

    def test_createtablespace(self):
        tbname = 'tester'
        self.fs.create_tablespace(tbname)
        tbs = os.path.join(self.fsDir, 'tsData/' + tbname)
        self.assertEqual(True, os.path.exists(tbs))

    def test_add_table_check_DSMap(self):
        CreateTable('newtab', 'name str', self.fs).execute()
        expected_map = {'newtab': [('name', 'str')]}
        self.assertEqual(expected_map, self.fs.show_tables())


if __name__ == '__main__':
    unittest.main()
