import unittest
from bitarray import bitarray

from src.Table.RowBlock import *
from src.Table.Column import Column


class Test_RowBlocks(unittest.TestCase):

    def setUp(self):
        li = [Column('name', 'str'), Column('age', 'int'), Column('ismarried', 'bool'),
              Column('gpa', 'float')]
        self.rowblock = RowBlock(li)

    def test_intconversion(self):
        self.assertEqual('00000000000000000000000000000001', self.rowblock.convert_int(1))

    def test_negintconversion(self):
        self.assertEqual('10000000000000000000000000000001', self.rowblock.convert_int(-1))

    def test_floatconversion(self):
        self.assertEqual('0000000100000000000000000000000000000001', self.rowblock.convert_float(0.01))

    def test_floatconversion2(self):
        self.assertEqual('0000000100000000000001100111010111111110', self.rowblock.convert_float(4234.22))

    def test_floatconversion3(self):
        self.assertEqual('0000001000000000000000000010011100001111', self.rowblock.convert_float(.999999))

    def test_floatconversion4(self):
        self.assertEqual('1000001000000001011000100111111101000011', self.rowblock.convert_float(-2323.2323232323))

    def test_strconversion(self):
        self.assertEqual('00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000001101000'
                         '01100101011011000110110001101111', self.rowblock.convert_string('hello'))

    def test_strconversion2(self):
        self.assertEqual('00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000111010001101000'
                         '01101001011100110010000001101001'
                         '01110011001000000110000100100000'
                         '01110100011001010111001101110100'
                         '00100000011011010110010101110011'
                         '01110011011000010110011101100101', self.rowblock.convert_string('this is a test message'))

    def test_strinvalid(self):
        with self.assertRaises(StringTooLong):
            self.rowblock.convert_string('weoifjwoiefjwoeifjwioefjweiofjwefoijwefoiwjefiowejfoiwefjwoiefjwoiefjwefiowf')

    def test_sizeblock(self):
        self.assertEqual(329, self.rowblock.get_blocksize())

    def test_entireblock(self):
        data = {'name': 'efef', 'age': 23, 'ismarried': True, 'gpa': 4.00}
        self.assertEqual('00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '00000000000000000000000000000000'
                         '01100101011001100110010101100110'
                         '00000000000000000000000000010111'
                         '10000000000000000000000000000000'
                         '000000100', self.rowblock.return_binaryblock(data))

    def test_binary_to_pydata(self):
        data = '00000000000000000000000000000000' \
               '00000000000000000000000000000000' \
               '00000000000000000000000000000000' \
               '00000000000000000000000000000000' \
               '00000000000000000000000000000000' \
               '00000000000000000000000000000000' \
               '00000000000000000000000000000000' \
               '01100101011001100110010101100110' \
               '00000000000000000000000000010111' \
               '10000000000000000000000000000000' \
               '000000100'
        outputmapping = {'name': ['efef'], 'age': [23], 'ismarried': [True], 'gpa': [4.00]}
        self.assertEqual(outputmapping, self.rowblock.bits_to_data(bitarray(data)))
