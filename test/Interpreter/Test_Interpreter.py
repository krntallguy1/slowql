import unittest
from src.core.SlowQLInterpreter import *


class Test_Interpreter(unittest.TestCase):

    def setUp(self):
        self.interpreter = SlowQLInterpreter()

    def test_find_subqueries_1(self):
        input = 'select * from daniel where age > [select age from ashley]'
        out = self.interpreter.find_subqueries(input)
        self.assertEqual('select * from daniel where age > subq_0', out)

    def test_find_subqueries_2(self):
        input = 'select * from daniel where age > [select age from ashley] ' \
                'where name = [select name from jim]'
        out = self.interpreter.find_subqueries(input)
        self.assertEqual('select * from daniel where age > subq_0 where name = subq_1', out)

    def test_find_subqueries_3(self):
        input = 'select * from daniel where age > [select age from ashley] ' \
                'where name = [select name from jim ' \
                'where name = [select name from calvin]]'
        out = self.interpreter.find_subqueries(input)
        self.assertEqual('select * from daniel where age > subq_0 where name = subq_1', out)
