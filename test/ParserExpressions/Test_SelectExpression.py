import unittest
from src.core.SlowQLInterpreter import *


class Test_SelectExpression(unittest.TestCase):

    def setUp(self):
        self.interpreter = SlowQLInterpreter()
        self.selectexpression = Select()
        self.staticnode1 = Static_Expression('first')
        self.staticnode2 = Static_Expression('second')
        self.fromnode = From()

    def tearDown(self):
        self.selectexpression = None
        self.staticnode1 = None
        self.staticnode2 = None
        self.fromnode = None

    def test_expect_chain(self):
        self.assertEqual(self.selectexpression.expect(), ['static', 'from'])

    def test_keyword(self):
        self.assertEqual(self.selectexpression.get_type(), 'select')

    def test_append_success_1(self):
        """
        case is:
        [select node] => [static node] => [from node] => [static node]
        """
        self.selectexpression = self.selectexpression.append(self.staticnode1.append(
            self.fromnode.append(self.staticnode2)))
        self.assertIsInstance(self.selectexpression, Select)

    def test_append_fail_1(self):
        """
        case is:
        [select node] => [from node] => [static node]
        expect InterpretError
        """
        with self.assertRaises(InterpretError):
            self.selectexpression = self.selectexpression.append(self.fromnode.append(self.staticnode2))
            self.interpreter.validate_chain(self.selectexpression)


if __name__ == '__main__':
    unittest.main()
