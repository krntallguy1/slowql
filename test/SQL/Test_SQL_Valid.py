import unittest

from src.core.SlowQL import *


class Test_SQL_Valid(unittest.TestCase):
    """
    General notes:
    1. Test for generic SQL VALIDITY
    2. This is not for functionality
    3. Test for edge cases for the Interpreter
    """
    d = SlowQL.get_instance()
    d.runSQL('Create table Daniel1(name str, age int)')
    d.runSQL('Create table Ashley(name str, age int)')
    d.runSQL('Create table jeremy(name str)')
    d.runSQL('Create table nim(name str, age int, graduated bool, salary float)')

    def test_query_1(self):
        """
        SQL: 'Select * from Daniel1'
        """
        try:
            self.d.runSQL('Select * from Daniel1')
        except Exception:
            raise

    def test_query_2(self):
        """
        SQL: 'Select Daniel1.name from Daniel1 Inner join nim on name'
        """
        try:
            self.d.runSQL('Select Daniel1.name from Daniel1 Inner join nim on name')
        except Exception:
            raise

    def test_query_3(self):
        """
        SQL: 'Select Daniel1.name from Daniel1 where age < 100'
        """
        try:
            self.d.runSQL('Select Daniel1.name from Daniel1 where age = 100')
        except Exception:
            raise

    def test_query_4(self):
        """
        SQL: 'Select Daniel1.name from Daniel1 inner join Ashley on name where Daniel1.age < 100'
        """
        try:
            self.d.runSQL('Select Daniel1.name from Daniel1 inner join Ashley on name where Daniel1.age = 100')
        except Exception:
            raise

    def test_query_5(self):
        """
        SQL:
        'Select Daniel1.name from Daniel1
            inner join Ashley
            on name
            where Ashley.age < 100
            inner join Jeremy
            on name
            where Daniel1.age < 100'
        """
        try:
            self.d.runSQL(
                "Select Daniel1.name from Daniel1 "
                "inner join Ashley "
                "on name "
                "where age = 100 "
                "inner join Jeremy "
                "on name "
                "where Daniel1.age = 100")
        except Exception:
            raise

    ######### FAILING QUERIES ##########
    def test_query_fail_1(self):
        """
        SQL: 'Select * from'
        Should fail due to lack of static node following from
        """
        with self.assertRaises(InterpretError):
            self.d.runSQL('Select * from')

    def test_query_fail_2(self):
        """
        SQL: 'Select from Daniel1'
        Should fail due to lack of static node following Select
        """
        with self.assertRaises(InterpretError):
            self.d.runSQL('Select from Daniel1')

    def test_query_fail_3(self):
        """
        SQL: 'seLect * from Daniel1 inner join nim on'
        Should fail due to lack of static node following 'on'
        """
        with self.assertRaises(InterpretError):
            self.d.runSQL('seLect * from Daniel1 inner join nim on')


if __name__ == '__main__':
    unittest.main()
    d.FS._delete_fs()
